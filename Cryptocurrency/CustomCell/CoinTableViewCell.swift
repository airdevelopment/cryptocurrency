//
//  CoinTableViewCell.swift
//  Cryptocurrency
//
//  Created by Evgeniya Pervushina on 2/8/18.
//  Copyright © 2018 Evgeniya Pervushina. All rights reserved.
//

import UIKit

class CoinTableViewCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var lAbbreviationName: UILabel!
    @IBOutlet weak var lFullName: UILabel!
    @IBOutlet weak var lCost: UILabel!
    @IBOutlet weak var lDynamics: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.view.layer.cornerRadius = 8.0
        self.view.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
