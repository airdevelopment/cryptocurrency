//
//  ExchangeViewController.swift
//  Cryptocurrency
//
//  Created by Evgeniya Pervushina on 2/8/18.
//  Copyright © 2018 Evgeniya Pervushina. All rights reserved.
//

import UIKit

class ExchangeViewController: UIViewController, UITextFieldDelegate {

    var data :CoinModel!
    var rate :Double = 1.0
    
    @IBOutlet weak var vBackground: UIView!
    @IBOutlet weak var lTitle: UILabel!
    @IBOutlet weak var ivFirst: UIImageView!
    @IBOutlet weak var tfFirst: UITextField!
    @IBOutlet weak var ivSecond: UIImageView!
    @IBOutlet weak var tfSecond: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tfFirst.delegate = self
        tfSecond.delegate = self
        
        tfFirst.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfSecond.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        tfFirst.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.prepareView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: View
    func prepareView() {
        self.navigationItem.title = String(format: NSLocalizedString("Exchange Rate", comment: ""))
        
        self.vBackground.layer.cornerRadius = 8.0
        self.view.clipsToBounds = true
        
        if let coin = data {
            self.lTitle.text = coin.name
            self.ivSecond.image = UIImage(named: coin.symbol.lowercased())
            self.ivFirst.image = UIImage(named: "dollar_money_cash")
            self.rate = coin.priceUsd
        }
    }
    
    //MARK: UITextField
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text?.count == 0 {
            tfFirst.text = ""
            tfSecond.text = ""
        }
        if let text = textField.text {
            if textField.isEqual(tfFirst) {
                if let doubleValue = Double(text) {
                    let result = doubleValue / self.rate
                    tfSecond.text = String(result)
                }
            } else {
                if let doubleValue = Double(text) {
                    let result = doubleValue * self.rate
                    tfFirst.text = String(result)
                }
            }
        }
    }
    
    //MARK: Actions
    @IBAction func actionSwich(_ sender: Any) {
        if self.rate == data?.priceUsd {
            self.ivFirst.image = UIImage(named: "btc")
            self.rate = data.priceBtc
        } else {
            self.ivFirst.image = UIImage(named: "dollar_money_cash")
            self.rate = data.priceUsd
        }
        self.tfSecond.text = ""
        self.tfFirst.text = ""
    }
}
