//
//  Networking.swift
//  Cryptocurrency
//
//  Created by Evgeniya Pervushina on 2/8/18.
//  Copyright © 2018 Evgeniya Pervushina. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol ECNetworkProtocol {}

typealias NetworkingCompletionHandler = (JSON, NSError?) -> Void

let ECNetworkDomain = "ECNetworkErrorDomain"
let BaseUrl = "https://api.coinmarketcap.com/v1/"
let ticker = "ticker/"

enum ECNetworkErrorType : Int {
    
    case noInternet = 1
    case invalidToken = 2
    case accessTokenNotSet = 3
    case invalidRefreshToken = 4
    case invalidVerificationCode = 5
    case unknownNetworkError = 99
}

class Networking: NSObject {
    
    func getAllCoins(parameters: [String :String], completion: @escaping NetworkingCompletionHandler) {
        Alamofire.request(
            URL(string: BaseUrl + ticker)!,
            method: .get,
            parameters: parameters)
            .validate()
            .responseJSON { (response) -> Void in
                self.finishRequest(response:response, completionHandler: completion)
        }
    }
    
    // MARK: - common functional
    
    private func finishRequest(response: Alamofire.DataResponse<Any>, completionHandler: NetworkingCompletionHandler?) {
        switch response.result {
        case .success(_):
            
            if let value = response.result.value {
                let json = JSON(value)
                
                print("JSON: \(json)")

                let error = NSError.init(domain: ECNetworkDomain, code: ECNetworkErrorType.unknownNetworkError.rawValue, userInfo: nil)
                if (completionHandler == nil) {
                    completionHandler!(JSON.null, error)
                    return
                }
                
                
                if (completionHandler != nil) {
                    completionHandler!(json, nil)
                }
            }
        case .failure(let error):
            
            print(error)
            if (completionHandler != nil) {
                completionHandler!(JSON.null, error as NSError)
            }
        }
    }
}

