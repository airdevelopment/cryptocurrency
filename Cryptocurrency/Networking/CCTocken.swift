//
//  CCTocken.swift
//  Cryptocurrency
//
//  Created by Evgeniya Pervushina on 2/8/18.
//  Copyright © 2018 Evgeniya Pervushina. All rights reserved.
//

import UIKit

class CCTocken: NSObject {
    
    var refreshToken: String?
    var accessToken: String?
    var tokenType : String?
    var scope : String?
    var expiresIn : Int?
    
    init(refreshToken: String!, accessToken: String!, tokenType: String!, scope: String!, expiresIn : Int) {
        self.refreshToken = refreshToken
        self.accessToken = accessToken
        self.tokenType = tokenType
        self.scope = scope
        self.expiresIn = expiresIn
        
        super.init()
    }
}
