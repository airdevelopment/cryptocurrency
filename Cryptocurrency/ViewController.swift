//
//  ViewController.swift
//  Cryptocurrency
//
//  Created by Evgeniya Pervushina on 2/8/18.
//  Copyright © 2018 Evgeniya Pervushina. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DataManagerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var dataManager = DataManager()
    var data = [CoinModel]()
    let cellIdentifier = "CoinTableViewCell"
    let segueIdentifier = "toChangeSegue"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = String(format: NSLocalizedString("Сryptocurrencies", comment: ""))
        
        self.tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        self.dataManager.delegate = self
        self.dataManager.loadCoinsList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK: UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CoinTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! CoinTableViewCell!
        
        self.prepareViewFor(cell: cell, index: indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: segueIdentifier, sender: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    //MARK: PrepareView
    func prepareViewFor(cell: CoinTableViewCell, index:NSInteger) {
        cell.ivIcon.image = UIImage(named: self.data[index].symbol.lowercased())
        cell.lFullName.text = self.data[index].name
        cell.lAbbreviationName.text = self.data[index].symbol
        cell.lCost.text = "$ " + String(self.data[index].priceUsd)

        let percentChange1h = self.data[index].percentChange1h
        cell.lDynamics.text = percentChange1h
        cell.lDynamics.textColor = self.percentTextColor(persentString:percentChange1h)
    }
    
    func percentTextColor(persentString: String) -> UIColor {
        if let cost = Double(persentString) {
            if cost < 0 {
                return UIColor.red
            }
            return UIColor.green
        }
        return UIColor.clear
    }
    
    //MARK: DataManagerDelegate
    func setData(data: [CoinModel]) {
        self.data = data
    }
    
    func updateTableView() {
        self.tableView.reloadData()
    }
    
    //MARK: PrepareSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifier {
            if let indexPath = tableView.indexPathForSelectedRow{
                let detailVC = segue.destination as! ExchangeViewController
                detailVC.data = self.data[indexPath.row]
            }
        }
    }
}

