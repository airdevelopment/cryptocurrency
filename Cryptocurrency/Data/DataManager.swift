//
//  DataManager.swift
//  Cryptocurrency
//
//  Created by Evgeniya Pervushina on 2/8/18.
//  Copyright © 2018 Evgeniya Pervushina. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias DataManagerComletionHandler = ([CoinModel], NSError?) -> Void
typealias JSONDictionary = [String:Any]

protocol DataManagerDelegate:class {
    func setData(data:[CoinModel])
    func updateTableView()
}

class DataManager: NSObject {
    weak var delegate:DataManagerDelegate?
    var networking = Networking()
    
    func loadCoinsList() {
        let parameters = [String:String]()
        
        self.networking.getAllCoins(parameters: parameters, completion: { (json:JSON, error: NSError?) -> Void in
            
            guard error == nil else {
                //error alert
                return
            }
            
            let array = self.dictionaryFromJson(json:json)
            
            self.delegate?.setData(data: array)
            self.delegate?.updateTableView()
        })
    }
    
    func dictionaryFromJson(json:JSON) -> [CoinModel] {
        if let arr = json.array {
            var mutDict = [CoinModel]()
            for var element in arr {
                let oneCoin = CoinModel()
                let coin = oneCoin.initWithJson(json:element)
                mutDict.append(coin)
            }
            
            return mutDict
        }
        
        return [CoinModel]()
    }
}
