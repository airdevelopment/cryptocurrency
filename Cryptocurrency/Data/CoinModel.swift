//
//  Coin.swift
//  Cryptocurrency
//
//  Created by Evgeniya Pervushina on 2/8/18.
//  Copyright © 2018 Evgeniya Pervushina. All rights reserved.
//

import UIKit
import SwiftyJSON

class CoinModel: NSObject {
    @objc dynamic var coinId:           NSNumber = 0
    @objc dynamic var name:             String = ""
    @objc dynamic var symbol:           String = ""
    @objc dynamic var rank:             NSNumber = 0
    @objc dynamic var priceUsd:         Double = 0.0
    @objc dynamic var priceBtc:         Double = 0.0
    @objc dynamic var volume24Usd:      String = ""
    @objc dynamic var marketCapUsd:     String = ""
    @objc dynamic var availableSupply:  String = ""
    @objc dynamic var totalSupply:      String = ""
    @objc dynamic var percentChange1h:  String = ""
    @objc dynamic var percentChange24h: String = ""
    @objc dynamic var percentChange7d:  String = ""
    @objc dynamic var last_updated:     String = ""
    
    func initWithJson(json: JSON) -> CoinModel {
        if let id = json["id"].rawString(),
            let name = json["name"].rawString(),
            let symbol = json["symbol"].rawString(),
            let rank = json["rank"].rawString(),
            let price_usd = json["price_usd"].rawString(),
            let price_btc = json["price_btc"].rawString(),
            let volume24Usd = json["24h_volume_usd"].rawString(),
            let market_cap_usd = json["market_cap_usd"].rawString(),
            let available_supply = json["available_supply"].rawString(),
            let total_supply = json["total_supply"].rawString(),
            let percent_change_1h = json["percent_change_1h"].rawString(),
            let percent_change_24h = json["percent_change_24h"].rawString(),
            let percent_change_7d = json["percent_change_7d"].rawString(),
            let last_updated = json["last_updated"].rawString()
            
        {
            self.coinId = NSNumber(value:(id as NSString).floatValue)
            self.name = name
            self.symbol = symbol
            self.rank = NSNumber(value:(rank as NSString).floatValue)
            self.priceUsd = (NumberFormatter().number(from: price_usd)?.doubleValue)!
            self.priceBtc = (NumberFormatter().number(from: price_btc)?.doubleValue)!
            self.volume24Usd = volume24Usd
            self.marketCapUsd = market_cap_usd
            self.availableSupply = available_supply
            self.totalSupply = total_supply
            self.percentChange1h = percent_change_1h
            self.percentChange24h = percent_change_24h
            self.percentChange7d = percent_change_7d
            self.last_updated = last_updated
        }
        
        return self
    }
}









